module Colour where

-- Colour   :: Biritsh English 
-- Color    :: American English

import Graphics.Gloss.Data.Color (Color, makeColor)
import Data.Tuple.Extra (uncurry3)
import Numeric (showHex)
import Data.Char (digitToInt, isHexDigit)
import Data.List.Extra (chunksOf)


-- Interface --------------------

type ColNumb = Float -- If i want double instead at some point

class ColourConvertable a where            --Change to ColourEncoding | Add HEX ??
    toRGB       :: a -> RGB
    fromRGB     :: RGB -> a
    toLAB       :: a -> OKLAB
    fromOKLAB   :: OKLAB -> a

    toTuple     :: a -> (ColNumb,ColNumb,ColNumb)
    fromTuple   :: (ColNumb,ColNumb,ColNumb) -> a 

    fromHex     :: String -> Maybe a
    toHex       :: a -> String
    real        :: a -> Color
    

-- RGB ---------------------------------

data RGB = RGB ColNumb ColNumb ColNumb
            deriving (Show, Eq)

instance ColourConvertable RGB where 
    toRGB = id
    toLAB = rgbToOklab 

    toTuple (RGB r g b)   = (r, g, b)
    fromTuple (x,y,z) = RGB x y z

    toHex = colourToHex
    fromHex = hexToColour

    real c      = uncurry3 makeColor (toTuple c)  1.0

    --unimplemented
    fromOKLAB = undefined
    fromRGB = undefined


-- OKLAB ----------------------------------
-- More perception accurate
-- Not quite isometric with RGB

-- Man i did some research and OKLAB is non-euclidean : ))))))
-- No wonder the conversion is so bizzare
-- 10 may 2024

data OKLAB = OKLAB ColNumb ColNumb ColNumb
                deriving (Show, Eq)

instance ColourConvertable OKLAB where 

    toRGB = oklabToRgb
    toLAB = id

    toTuple (OKLAB l a b) = (l, a, b)
    fromTuple (x,y,z) = OKLAB x y z

    real c = real $ toRGB c

    toHex = colourToHex . toRGB

    fromHex h = do {
                        rgb <- hexToColour h;
                        return $ toLAB rgb 
                    }

    --unimplemented
    fromOKLAB = undefined
    fromRGB = undefined

-- Hex functions -------------------------------------------

-- Colour -> Hex

channelToBase16 :: Float -> Int
channelToBase16 f = max 0 (min 255 $ round (f * 255))

channelToHex :: Float -> String
channelToHex f = let 
                    i = channelToBase16 f
                 in
                    if i <= 15 then '0' : showHex i ""
                    else showHex i ""

colourToHex :: ColourConvertable c => c -> String
colourToHex c = let RGB x y z = toRGB c in channelToHex x ++ channelToHex y ++ channelToHex z  
-- inefficient, but the string will never exceed 6 characters -- will remain O(1) because you can't scale it


-- Hex -> Colour

channelHexToFloat :: String -> Float
channelHexToFloat col  = fromIntegral (r (reverse col)) / 255
                where r [] = 0
                      r (c:cs) = digitToInt c + 16 * r cs

hexToColour :: String -> Maybe RGB
hexToColour s | length s == 6 && filter isHexDigit s == s   = let 
                                                                floatList = take 3 $ map channelHexToFloat (chunksOf 2 s)
                                                                r = head floatList
                                                                g = head (tail floatList)
                                                                b = last floatList
                                                              in 
                                                                Just $ RGB r g b
              | otherwise                                   = Nothing 

-- RGB & OKLAB conversion

rgbToOklab :: RGB -> OKLAB
rgbToOklab (RGB r g b)  = let 
                                l = 0.4122214708 * r + 0.5363325363 * g + 0.0514459929 * b
                                m = 0.2119034982 * r + 0.6806995451 * g + 0.1073969566 * b
                                s = 0.0883024619 * r + 0.2817188376 * g + 0.6299787005 * b

                                l_ = cubeRoot l
                                m_ = cubeRoot m
                                s_ = cubeRoot s

                                _l = 0.2104542553*l_ + 0.7936177850*m_ - 0.0040720468*s_
                                _a = 1.9779984951*l_ - 2.4285922050*m_ + 0.4505937099*s_
                                _b = 0.0259040371*l_ + 0.7827717662*m_ - 0.8086757660*s_
                          in
                            OKLAB _l _a _b

-- OKLAB is non-euclidean
-- Conversion is correct
-- But the colour picker 
-- does not have support to manipulate OKLAB as desired
oklabToRgb :: OKLAB -> RGB
oklabToRgb (OKLAB l a b) = let
                                _l = l + 0.3963377774 * a + 0.2158037573 * b;
                                _m = l - 0.1055613458 * a - 0.0638541728 * b;
                                _s = l - 0.0894841775 * a - 1.2914855480 * b;    
        
                                l_ = _l ** 3
                                m_ = _m ** 3
                                s_ = _s ** 3

                                red =   min 1 $ max 0 $  4.0767416621 * l_ - 3.3077115913 * m_ + 0.2309699292 * s_
                                green = min 1 $ max 0 $  -1.2684380046 * l_ + 2.6097574011 * m_ - 0.3413193965 * s_
                                blue =  min 1 $ max 0 $  -0.0041960863 * l_ - 0.7034186147 * m_ + 1.7076147010 * s_
                           in
                            RGB red green blue

cubeRoot :: Float -> Float
cubeRoot = (** (1/3)) -- has inaccuracies, find better method
      
-- Example Colours

lavender :: RGB
lavender = RGB 0.9 0.9 0.95

darkLavender :: RGB
darkLavender = RGB 0.3 0.25 0.45

red' :: RGB 
red' = RGB 1.0 0.0 0.0

blue' :: RGB
blue' = RGB 0.0 0.0 1.0

lightYellow :: RGB
lightYellow = RGB 1.0 1.0 0.85

