module Items where

import GeometryExtention
import Graphics.Gloss.Data.Picture
import Colour
import Data.Char

-- Slider -----------------------------------------------------
data FloatSlider = FloatSlider {  bounds :: Rectangle,
                                  position :: Point, -- change to different name?
                                  minVal :: Float,
                                  maxVal :: Float,
                                  value :: Float,
                                  title :: String
                                }

-- Possible addition ? Slider = FloatSlider {} | ColourSlider { col :: [c] }

getSliderValue :: FloatSlider -> Point -> Float
getSliderValue slider point | inRectangle (bounds slider) (point <-> position slider)  = let localX       = fst $ point <-> position slider
                                                                                             xSize        = width $ bounds slider
                                                                                             percent      = localX / xSize
                                                                                             range        = maxVal slider - minVal slider
                                                                                          in range * percent
                            | otherwise = value slider

getSliderValuePercent :: FloatSlider -> Float
getSliderValuePercent s = (value s / (maxVal s - minVal s))


colourChannelReplace ::  ColourConvertable c => c -> (Bool,Bool,Bool) -> Float -> c
colourChannelReplace c (x, y, z) newVal = let   (c1, c2, c3) = toTuple c
                                                f v b = if b then newVal else v
                                                finalTup = (f c1 x, f c2 y, f c3 z)
                                            in
                                                fromTuple finalTup

-- HEXCODE Input Box ------------------------------------------------------------------------------
data HexBox = HexBox {hexPosition :: Point,
                      contents :: String}

initHexBox :: Point -> HexBox
initHexBox p = HexBox p ""

writeToHex :: HexBox -> Char -> HexBox
writeToHex (HexBox p s) c | isHexDigit c && length s < 6   = HexBox p (s ++ [c])
                          | otherwise                      = HexBox p s

colourToHexBox :: ColourConvertable c => HexBox -> c -> HexBox
colourToHexBox box c = box{contents = colourToHex (toRGB c)}

backspaceHex :: HexBox -> HexBox
backspaceHex (HexBox p "") = HexBox p ""
backspaceHex (HexBox p s) = HexBox p (init s)

hexBoxBounds :: Rectangle
hexBoxBounds = Rectangle 75 30
