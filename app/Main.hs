module Main where

import Colour
import Graphics.Gloss.Interface.IO.Interact
import Enviroment
import Data.Char (toUpper)

-- Baseic rendering constants
screenSize :: (Int, Int)
screenSize = (240,300)

window :: Display
window = InWindow "ColourPicker" screenSize (100,100)

bgColour :: Color
bgColour = real (RGB 0.7 0.7 0.7)

-- Colour picker setups
runRGB :: IO () 
runRGB = interactIO window (real lavender) initialEnvRGB (return . drawEnviroment) colourPickerEventHandler (\_ -> pure ())

runOKLAB :: IO () 
runOKLAB = interactIO window (real lavender) initialEnvOKLAB (return . drawEnviroment) colourPickerEventHandler (\_ -> pure ())

-- Main function
main :: IO ()
main = do {
        
        putStrLn "*************************************************";
        putStrLn "| Choose colour space";
        putStrLn "|     (Note: OKLAB is not implemented correctly)";
        putStrLn "| RGB | OKLAB | (Default: RGB)";
        putStrLn "*************************************************";

        i <- getLine ;
        
        let 
            f l = 
                case map toUpper l of
                    "RGB"       -> runRGB
                    "OKLAB"     -> runOKLAB
                    ""          -> runRGB
                    _           -> do {putStrLn "| Please select valid option \n| Leave empty for default"; n <- getLine; f n}
        in
        
        f i 
    }