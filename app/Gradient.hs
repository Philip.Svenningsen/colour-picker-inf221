module Gradient where

import Colour

-- Colour
colGradient :: ColourConvertable c => c -> c -> Int -> [c]
colGradient c1 c2 count = let
    (r1, g1, b1) = toTuple c1
    (r2, g2, b2) = toTuple c2
    in map fromTuple $ zip3 (fGradient r1 r2 count) (fGradient g1 g2 count) (fGradient b1 b2 count)

-- possible improvement : fmap fromRational
fGradient :: Float -> Float -> Int -> [Float]
fGradient f1 f2 steps = 
    let step = (f2 - f1) / (fromIntegral (steps-1) :: Float)
    in [f2,(f2-step) .. f1] 
