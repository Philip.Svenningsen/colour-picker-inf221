module GeometryItems where

import Graphics.Gloss hiding (Rectangle)
import Colour
import GeometryExtention
import Gradient
import Items


-- Gradient

drawGradient :: ColourConvertable c => Float -> Float -> c -> c -> Picture
drawGradient w h c1 c2 = pictures $ gradientLines w h $ colGradient c1 c2 $ floor w

gradientLines :: ColourConvertable a => Float -> Float -> [a] -> [Picture]
--                      Col           -> Line                     Append to rest
gradientLines w h (c:cs) | w < 1.0    = []
                       | otherwise  = color (real c) (line [(w,0.0), (w,h)]) : gradientLines (w-1.0) h cs
gradientLines _ _ _   = []


-- Slider

showSlider :: FloatSlider -> Picture
showSlider s = let
                   rect = bounds s
                   rectPath = move (position s) (Color black $ lineLoop $ rectangleToPath rect) 
                   --gradient = move (position s) (drawGradient (width rect) (height rect) red' blue') 
                   xOffset = width rect * getSliderValuePercent s
                   line' = Line [position s <+> (xOffset, 0), position s <+> (xOffset, height rect)]
                   label = move ((position s) <+> (0,5 + height rect)) $ fontSmall $ title s ++ " : " ++ show (value s)
               in  Pictures [label, rectPath, Color black line']


    -- Kind of a scrapped together solution, but it works
sliderConstrainedGradient :: ColourConvertable c => FloatSlider -> (Bool, Bool, Bool) -> c -> Picture
sliderConstrainedGradient s bools col = let
                                            minColour = colourChannelReplace col bools (minVal s)
                                            maxColour = colourChannelReplace col bools (maxVal s)
                                            rect = bounds s
                                        in
                                        move (position s) $ drawGradient (width rect) (height rect) minColour maxColour


-- HexBox

showHexBox :: HexBox -> Picture
showHexBox (HexBox p s) = move p $ Pictures [(lineLoop $ rectangleToPath hexBoxBounds), move (2,9) $ fontMedium s ]
