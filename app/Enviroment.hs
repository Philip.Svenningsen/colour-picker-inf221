module Enviroment where

import Colour
import GeometryExtention
import GeometryItems
import Items
import Graphics.Gloss.Interface.IO.Interact
import System.Hclip -- double check licence!
import Style (colFish, triangleSize)


data ColourPickerEnv c = ColourPickerEnv {  sliderX :: FloatSlider, 
                                            sliderY :: FloatSlider,
                                            sliderZ :: FloatSlider,
                                            hexContainer :: HexBox,
                                            pickedColour :: c }

initialEnvRGB :: ColourPickerEnv RGB
initialEnvRGB = ColourPickerEnv 
                    baseSlider{position=(-100,60)  ,title="Red"} 
                    baseSlider{position=(-100,00)   ,title="Green"} 
                    baseSlider{position=(-100,-60)    ,title="Blue"} 
                    (initHexBox (-100,-110))
                    (RGB 1 1 1)

initialEnvOKLAB :: ColourPickerEnv OKLAB
initialEnvOKLAB = ColourPickerEnv 
                    baseSlider{position=(-100,60)   ,title="Lightness"} 
                    baseSlider{position=(-100,-00)   ,title="Green/Red"} 
                    baseSlider{position=(-100,-60)  ,title="Blue/Yellow"} 
                    (initHexBox (-100,-110))
                    (OKLAB 1 1 1)


baseSlider :: FloatSlider
baseSlider = FloatSlider (Rectangle 200 30) origo 0 1 1 ""

colourPickerEventHandler :: ColourConvertable c => Event -> ColourPickerEnv c -> IO (ColourPickerEnv c)
colourPickerEventHandler (EventKey (MouseButton LeftButton) Down _ pos) env = 
    do {
        -- Funky return solution is not neccecary, could just let

        (r,g,b)       <- return (toTuple $ pickedColour env);
        x       <- return (getSliderValue (sliderX env){value = r} pos);
        y       <- return (getSliderValue (sliderY env){value = g} pos);
        z       <- return (getSliderValue (sliderZ env){value = b} pos);

        setClipboard (colourToHex $ fromTuple (x,y,z) `asTypeOf` pickedColour env); -- maybe move to descrete button

        return $ setEnviromentColour env (fromTuple (x,y,z))
    }

colourPickerEventHandler (EventKey (Char c) Down _ _) env = 
    do {
        let
            hex = writeToHex (hexContainer env) c

            newEnviroment  = case fromHex (contents hex) of
                                        Just a -> setEnviromentColour env a
                                        Nothing -> env{hexContainer = hex}
        in
        
        return newEnviroment
    }

colourPickerEventHandler (EventKey (SpecialKey KeyLeft) Down _ _) env = 
    do {
        let
            hex = backspaceHex (hexContainer env)

     
        in
        
        return $ env{hexContainer = hex}
    }

colourPickerEventHandler _ env = do { return env }

setEnviromentColour :: ColourConvertable c => ColourPickerEnv c -> c -> ColourPickerEnv c
setEnviromentColour env col = let   
                                        (x,y,z) = toTuple col
                                        sX = (sliderX env) {value = x}
                                        sY = (sliderY env) {value = y}
                                        sZ = (sliderZ env) {value = z}
                                        hex = colourToHexBox (hexContainer env) col
                                      in 
                                        ColourPickerEnv sX sY sZ hex col


drawEnviroment :: ColourConvertable c => ColourPickerEnv c -> Picture
drawEnviroment env = Pictures $ 

                            (move (100+triangleSize*2-2,100) $ colFish (RGB 1 1 1) (darkLavender)) :
                            (showHexBox $ hexContainer env) : 
                            (sliderConstrainedGradient (sliderX env) (True, False, False) (pickedColour env)) :
                            (sliderConstrainedGradient (sliderY env) (False, True, False) (pickedColour env)) :
                            (sliderConstrainedGradient (sliderZ env) (False, False, True) (pickedColour env)) :
                            (showSlider $ sliderX env) :  
                            (showSlider $ sliderY env) : 
                            (showSlider $ sliderZ env) : 
                            let 
                                colourRectPath = rectangleToPath $ Rectangle 80 30
                            in
                            (move (20,-110) $ Pictures [(Color (real $ pickedColour env) $ Polygon colourRectPath), lineLoop colourRectPath, move (-35,9) $ fontMedium "->" ]) : []