module Style where

import GeometryExtention
import Colour
import Graphics.Gloss.Data.Picture

-- Post deadline addition

triangleSize :: Float
triangleSize = -15

fishTriangle :: Picture
fishTriangle = Polygon [(0,0), (0,triangleSize), (triangleSize,0)]

fishEye :: Picture
fishEye = let e = circleSolid 2.3 in Color (real darkLavender) $ move (triangleSize/3.5,triangleSize/3.5) e

colFish :: ColourConvertable c => c -> c -> Picture
colFish c1 c2 = Pictures [move (-1,-1) $ fish c2, fish c1 ]

fish :: ColourConvertable c => c -> Picture
fish c = move (-triangleSize*2,-triangleSize*2) $ Pictures $ [ color (real c) $ move (x,y) fishTriangle | x <- [0,triangleSize], y <- [0,triangleSize]] ++ [fishEye]