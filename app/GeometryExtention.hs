module GeometryExtention where

import Graphics.Gloss hiding (Rectangle)
import Graphics.Gloss.Data.Point (pointInBox)



-- Point Extention
(<+>) :: Point -> Point -> Point 
(<+>) (x1,y1) (x2,y2) = (x1+x2,y1+y2)

(<->) :: Point -> Point -> Point
(<->) (x1,y1) (x2,y2) = (x1-x2,y1-y2)

-- the fact that you cannot treat a Point in GLOSS as a vector is criminal

move :: Point -> Picture -> Picture
move pos pic = uncurry Translate pos pic

origo :: Point 
origo = (0,0)

-- Rectangle
data Rectangle = Rectangle {width :: Float, height :: Float}

inRectangle :: Rectangle -> Point -> Bool
inRectangle (Rectangle w h) p = pointInBox p origo (w,h)

rectangleAsPoint :: Rectangle -> Point
rectangleAsPoint (Rectangle w h) = (w,h)

rectangleToPath :: Rectangle -> Path
rectangleToPath (Rectangle w h) = [origo, origo <+> (w,0.0), origo <+> (w,h), origo <+> (0.0,h)]

-- Text Extention
-- Font: a specified size and typeface
fontMedium :: String -> Picture
fontMedium s = Scale 0.15 0.15 $ Text s

fontSmall :: String -> Picture
fontSmall s = Scale 0.1 0.1 $ Text s


