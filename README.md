# Colour Fish - Colour Picker 

<img src="assets/readme-images/colourfish-icon.png" alt="colour fish icon" width="80"/>

Basic Haskell GLOSS Colour Picker

## Colour Spaces
```
RGB     - Working
OKLAB   - Unfinished
```
[More on OKLAB](https://bottosson.github.io/posts/oklab/)

## Use 
```
Basic Use:
Sliders (a)  ->  LMB (Left Mouse Button)
Hex (b)      ->  (0..9) (A..F) (Left Arrow Key)

(!) Gloss doesn't register backspace corretly
    so left arrow key is a temporary solution

 -  Colour HEX code will be copied into 
    your clipboard when used
```

## Running
(Requires Haskell Cabal)

```
... Colour-Picker-INF221> Cabal Run
```

## Example

<img src="assets/readme-images/colourpicker-guide.png" alt="colour picker guide" width="300"/>


## Codebase overview

### Missing
* Smart constructors
* Repa & QuickCheck integration

### Code overview

```Haskell
module Colour where

data RGB    = RGB   Float Float Float
data OKLAB  = OKLAB Float Float Float

class ColourConvertable a where
    toRGB       :: a -> RGB
    fromRGB     :: RGB -> a
    toLAB       :: a -> OKLAB
    fromOKLAB   :: OKLAB -> a

    toTuple     :: a -> (Float, Float, Float)
    fromTuple   :: (Float, Float, Float) -> a 

    fromHex     :: String -> Maybe a
    toHex       :: a -> String

    real        :: a -> Color


----------------------------------------------
module GeometryExtention where

(<+>) :: Point -> Point -> Point 
(<+>) (x1,y1) (x2,y2) = (x1+x2,y1+y2)

(<->) :: Point -> Point -> Point
(<->) (x1,y1) (x2,y2) = (x1-x2,y1-y2)

data Rectangle = Rectangle {width :: Float, height :: Float}


----------------------------------------------
module Items where

data FloatSlider = FloatSlider {  bounds :: Rectangle,
                                  position :: Point,
                                  minVal :: Float,
                                  maxVal :: Float,
                                  value :: Float,
                                  title :: String
                                }

data HexBox = HexBox {hexPosition :: Point,
                      contents :: String}


---------------------------------------------
module GeometryItems where

drawGradient :: ColourConvertable c => Float -> Float -> c -> c -> Picture

showSlider :: FloatSlider -> Picture

showHexBox :: HexBox -> Picture


----------------------------------------------
module Gradient where

colGradient :: ColourConvertable c => c -> c -> Int -> [c]


---------------------------------------------
module Enviroment where


data ColourPickerEnv c = ColourPickerEnv {  sliderX :: FloatSlider, 
                                            sliderY :: FloatSlider,
                                            sliderZ :: FloatSlider,
                                            hexContainer :: HexBox,
                                            pickedColour :: c }

```

## Lincence needs to be specified

<img src="assets/readme-images/cats-fighting.gif" alt="colour picker guide" width="150"/>

please wait for that